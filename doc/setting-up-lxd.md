# Setting up LXD

LXD (or Incus) needs to be configured a particular way before Vagrant can use
it.

Specifically, the following settings need to be applied:

 1. The server must allow HTTPS API access from your machine.
 2. The server must have a working network bridge.
 3. Your user must be in the "lxd" (or "incus-admin") group.
 4. Your user must have a client certificate registered.

Note the following instructions apply specifically to LXD. When using Incus,
you'll need to change the commands and group names accordingly.

## Ubuntu

### Focal and Later

On Ubuntu 20.04 and later, LXD is installed as a [Snap][]. To install
and configure it as described above, you can use the following commands:

```sh
# install lxd
sudo snap install lxd

# enable https api access
sudo lxd init --auto --network-address=127.0.0.1 --network-port=8443

# add your user to the lxd group
sudo usermod -a -G lxd $(whoami)

# apply the new group membership
newgrp lxd
```

Then, the first time you run Vagrant in a project that uses the LXD
provider, it will generate a client certificate for the plugin to use
and instruct you to add it to LXD with the following message:

```
You may need configure LXD to allow requests from this machine. The
easiest way to do this is to add your LXC client certificate to LXD's
list of trusted certificates. This can typically be done with the
following command:

    $ lxc config trust add ~/.vagrant.d/data/lxd/client.crt

You can find more information about configuring LXD at:

    https://documentation.ubuntu.com/lxd/en/latest/howto/initialize/
```

Once you run that command as instructed, everything should be set up for
the plugin to work correctly.

### Xenial and Earlier

To install LXD and configure it as described above on Ubuntu 16.04, you
can use the following commands:

```sh
# install lxd
sudo apt install -y lxd

# enable https api access
sudo lxd init --auto --network-address=127.0.0.1 --network-port=8443

# set up a network bridge (press enter to accept the default values)
sudo dpkg-reconfigure -p medium lxd

# add your user to the lxd group
sudo usermod -a -G lxd $(whoami)
```

Once LXD is configured, you should register a client certificate for
Vagrant to use when authenticating to the API (this command will
automatically generate the certificate for you):

```sh
# apply new group membership
newgrp lxd

# create and add a client certificate
lxc config trust add ~/.config/lxc/client.crt
```

At this point everything should be set up for the plugin to work
correctly.

## Other Platforms

The Canonical website has a [detailed guide][getting-started-cli]
to installing LXD on other platforms. The steps to configure LXD for
Vagrant will be similar to those above, but some commands will differ.

If you're using the plugin on another platform, please feel free to
propose an addition to this document or add instructions to the
project's [wiki][] for others to follow.

[Snap]: https://snapcraft.io/
[getting-started-cli]: https://documentation.ubuntu.com/lxd/en/latest/howto/initialize/
[wiki]: https://gitlab.com/catalyst-it/vagrant-lxd/wikis
