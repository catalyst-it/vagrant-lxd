#
# Copyright (c) 2022 Catalyst.net Ltd
#
# This file is part of vagrant-lxd.
#
# vagrant-lxd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# vagrant-lxd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-lxd. If not, see <http://www.gnu.org/licenses/>.
#

require 'lib/vagrant-lxd'
require 'lib/vagrant-lxd/driver'

describe VagrantLXD::Driver::ProjectMiddleware do
  describe 'class' do
    subject do
      described_class.create('default')
    end

    it { should be_a Class }
    it { should respond_to :create }
    its('ancestors') { should include Faraday::Middleware }
  end

  describe 'instance' do
    let(:app) { double('app') }
    let(:url) { URI('https://127.0.0.1:8443/1.0/foo') }
    let(:env) { Faraday::Env.new(:get, nil, url) }

    %w(default custom).each do |project|
      context "with project '#{project}'" do
        subject do
          described_class.create(project).new(app)
        end

        it 'adds query parameter' do
          app.should receive(:call) do |new_env|
            new_env.should_not be env
            new_env.url.should_not eq url
            new_env.url.should eq URI("#{url}?project=#{project}")
          end

          subject.call(env)
        end
      end
    end
  end
end
